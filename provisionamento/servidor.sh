#!/bin/bash
    
#Variaveis
DEPS_PACKAGES="apt-transport-https ca-certificates curl gnupg-agent software-properties-common python3 python3-pip vim openjdk-11-jre wget tree apt-transport-https ca-certificates curl gnupg lsb-release git"
PACKAGES="docker-ce docker-ce-cli containerd.io"
PIP_PACKAGES="ComplexHTTPServer ansible"

# Registrando timestamp provision
sudo date >> /var/log/vagrant_provision.log

validateCommand() {
  if [ $? -eq 0 ]; then
    echo "[OK] $1"
  else
    echo "[ERROR] $1"
    exit 1
  fi
}

# Setando permissão no arquivo de logs
sudo chown vagrant: /var/log/vagrant_provision.log

# Instalando Pacotes
export DEBIAN_FRONTEND=noninteractive
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get --allow-releaseinfo-change update -qq >/dev/null 2>>/var/log/vagrant_provision.log && \
  sudo apt-get update -qq -y >/dev/null 2>>/var/log/vagrant_provision.log && \
	sudo apt-get install -qq -y ${DEPS_PACKAGES} ${PACKAGES} >/dev/null 2>>/var/log/vagrant_provision.log

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose >/dev/null 2>>/var/log/vagrant_provision.log
sudo chmod +x /usr/local/bin/docker-compose

validateCommand "Instalacao de Pacotes"

# Configurando usuario Jenkins
sudo useradd -b /var/lib -m -G docker -r jenkins >/dev/null 2>>/var/log/vagrant_provision.log
sudo mkdir -p /var/lib/jenkins/.ssh/ >/dev/null 2>>/var/log/vagrant_provision.log
sudo touch /var/lib/jenkins/.ssh/authorized_keys >/dev/null 2>>/var/log/vagrant_provision.log
sudo chown -R jenkins: /var/lib/jenkins/.ssh/ >/dev/null 2>>/var/log/vagrant_provision.log

validateCommand "Configuracao Jenkins"

# Instalando Pacotes do Python
pip3 install -q ${PIP_PACKAGES} >/dev/null 2>>/var/log/vagrant_provision.log && \
        pip3 uninstall -y docker-py >/dev/null 2>>/var/log/vagrant_provision.log

validateCommand "Pacotes Python"

#Ativando Servico do Docker
systemctl enable docker &>/dev/null && \
        systemctl start docker >/dev/null 2>>/var/log/vagrant_provision.log

validateCommand "Ativando Docker e configurando logs"

#Subindo container Portainer
sudo mkdir -p /opt/portainer >/dev/null 2>>/var/log/vagrant_provision.log
sudo cd /opt/portainer >/dev/null 2>>/var/log/vagrant_provision.log
sudo cat <<EOF > docker-compose.yaml
version: '3'

services:
  portainer:
    image: portainer/portainer-ce:2.13.1-alpine
    container_name: portainer
    hostname: portainer
    restart: unless-stopped
    security_opt:
      - no-new-privileges:true
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./portainer-data:/data
    ports:
      - 9000:9000
EOF
sudo docker-compose up -d >/dev/null 2>>/var/log/vagrant_provision.log
validateCommand "Subindo portainer"