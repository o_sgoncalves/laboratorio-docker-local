# Laboratório - Docker Local

Repositório para armazenar um Laboratório de Docker em ambiente local, simples de usar para ambientes de testes.

## Dependências

Antes de configurar qualquer máquina virtual é necessário ter pré instalado os seguintes softwares:

* [Git](https://git-scm.com/download)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/downloads)

Instale cada software citado acima, reinicie sua máquina para aplicar as configurações, e siga para o próximo passo.

## Configuração do Laboratório

O Laboratório será criado utilizando o [Vagrant](https://www.vagrantup.com/). Ferramenta para criar e gerenciar ambientes virtualizados (baseado em Inúmeros providers) com foco em automação.

Nesse laboratórios, que está centralizado no arquivo [Vagrantfile](./Vagrantfile), será criada uma máquina com as seguintes características:

Nome       | vCPUs | Memoria RAM | IP            | S.O.¹           | Script de Provisionamento²
---------- |:-----:|:-----------:|:-------------:|:---------------:| -----------------------------
servidor   | 1     | 4096MB      | 192.168.56.10 | ubuntu/21.04    | [provisionamento/servidor.sh](provisionamento/servidor.sh)

## Criando o Laboratório 
----------------------

Para criar o laboratório é necessário fazer o `git clone` desse repositório e, dentro da pasta baixada realizar a execução do `vagrant up`, conforme abaixo:

```bash
git clone https://gitlab.com/o_sgoncalves/laboratorio-docker-local
cd laboratorio-docker-local
vagrant up
```

_O Laboratório **pode demorar**, dependendo da conexão de internet e poder computacional, para ficar totalmente preparado._

> Em caso de erro na criação das máquinas sempre valide se sua conexão está boa, os logs de erros na tela e, se necessário, o arquivo **/var/log/vagrant_provision.log** dentro da máquina que apresentou a falha.

## Acessando a máquina criada

Para acessar a máquina criada, **sempre esteja no diretório clonado**, e execute o comando:
```bash
vagrant ssh servidor
```

Todos os comandos podem ser executados normalmente neste terminal.

Para acessar o portainer criado, basta inserir em seu navegador o endereço [http://192.168.56.10:9000/](http://192.168.56.10:9000/).

Todos os laboratórios podem ser executados normalmente nesta máquina virtual.

## Dicas de uso do Vagrant

Por fim, para melhor utilização, abaixo há alguns comandos básicos do vagrant para gerencia das máquinas virtuais.

Comandos                | Descrição
:----------------------:| ---------------------------------------
`vagrant init`          | Gera o VagrantFile
`vagrant box add <box>` | Baixar imagem do sistema
`vagrant box status`    | Verificar o status dos boxes criados
`vagrant up`            | Cria/Liga as VMs baseado no VagrantFile
`vagrant provision`     | Provisiona mudanças logicas nas VMs
`vagrant status`        | Verifica se VM estão ativas ou não.
`vagrant ssh <vm>`      | Acessa a VM
`vagrant ssh <vm> -c <comando>` | Executa comando via ssh
`vagrant reload <vm>`   | Reinicia a VM
`vagrant halt`          | Desliga as VMs

> Para maiores informações acesse a [Documentação do Vagrant](https://www.vagrantup.com/docs)